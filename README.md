# pbws (pretty basic web server)

This is my toy web server. DO NOT USE for anything you care about, I write it for my own small projects, and learning.

## example
```rust
use pbws::{Request, Response, ServerBuilder};

#[tokio::main]
async fn main() {
    let server = ServerBuilder::new()
    .add_route("POST", "/headers", return_headers)
    .build("127.0.0.1", 8080).unwrap();

    let _ = server.run().await;
}

async fn return_headers(req: Request) -> Response {
    let mut response = Response::new();
    let mut body = String::new();
    
    for (key, value) in req.headers() {
        body.push_str(&format!("{}: {}\n", key, value));
    }
    response.set_body(body);

    response
}
```
