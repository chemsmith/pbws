use std::collections::HashMap;

pub struct Response {
    status: u16,
    version: String,
    headers: HashMap<String, String>,
    body: String,
}

impl Response {
    pub fn new() -> Response {
        Response {
            status: 200,
            version: "HTTP/1.1".to_string(),
            headers: HashMap::new(),
            body: String::new(),
        }
    }

    pub fn add_header(&mut self, name: impl Into<String>, value: impl Into<String>) {
        self.headers.insert(name.into(), value.into());
    }

    pub fn set_body(&mut self, body: impl Into<String>) {
        self.body = body.into();

        if self.body.is_empty() {
            self.add_header("Content-Length", "0");
        } else {
            self.add_header("Content-Length", (self.body.len() + 2).to_string());
        }
    }

    pub fn set_status(&mut self, status: u16) {
        self.status = status;
    }

    pub fn write(&self) -> Vec<u8> {
        let mut response = format!("{} {}\r\n", self.version, self.status);

        if self.status > 299 {
            response.push_str("Content-Length: 0\r\n\r\n");
            return response.into();
        }

        for (name, value) in &self.headers {
            response.push_str(&format!("{}: {}\r\n", name, value));
        }

        if !self.body.is_empty() {
            response.push_str("\r\n");
            response.push_str(&self.body);
        }

        response.push_str("\r\n");

        response.into()
    }
}

impl Default for Response {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod test {
    use crate::response::Response;

    #[test]
    fn test_response() {
        let mut response = Response::new();
        response.set_body("Hello");

        assert_eq!(response.status, 200);
        assert_eq!(response.headers.len(), 1);
        assert_eq!(response.headers.get("Content-Length").unwrap(), "7"); // 5 + 2 for \r\n
        assert_eq!(response.body, "Hello");
    }

    #[test]
    fn test_response_write() {
        let mut response = Response::new();
        response.set_body("Hello");

        let response_bytes = response.write();

        assert_eq!(
            String::from_utf8_lossy(&response_bytes),
            "HTTP/1.1 200\r\nContent-Length: 7\r\n\r\nHello\r\n"
        );
    }

    #[test]
    fn test_response_write_empty_body() {
        let mut response = Response::new();
        response.set_body("");

        let response_bytes = response.write();

        assert_eq!(
            String::from_utf8_lossy(&response_bytes),
            "HTTP/1.1 200\r\nContent-Length: 0\r\n\r\n"
        );
    }

    #[test]
    fn test_response_write_404_status_code() {
        let mut response = Response::new();
        response.set_status(404);
        response.set_body("");

        let response_bytes = response.write();

        assert_eq!(
            String::from_utf8_lossy(&response_bytes),
            "HTTP/1.1 404\r\nContent-Length: 0\r\n\r\n"
        );
    }
}