mod request;
mod response;

pub use crate::{request::Request, response::Response};
use std::{collections::HashMap, future::Future, pin::Pin, sync::Arc};
use tokio::{io::AsyncWriteExt, net::TcpListener};

type Handler = Box<
    dyn Fn(
            Request,
        ) -> Pin<
            Box<dyn Future<Output = Result<Response, Box<dyn std::error::Error>>> + Send + Sync>,
        > + Send
        + Sync,
>;

struct Routes {
    routes: HashMap<String, Handler>,
}

impl Routes {
    fn match_route(&self, request: &Request) -> &Handler {
        if let Some(handler) = self
            .routes
            .get(&format!("{}::{}", request.method(), request.path()))
        {
            return handler;
        }

        self.routes.get(&"default".to_string()).unwrap()
    }
}

pub struct Server {
    routes: Routes,
    address: String,
    port: u16,
}

impl Server {
    pub async fn run(self) -> Result<(), Box<dyn std::error::Error>> {
        let listener = TcpListener::bind(format!("{}:{}", self.address, self.port))
            .await
            .unwrap();

        let routes = Arc::new(self.routes);

        loop {
            if let Ok((mut stream, _)) = listener.accept().await {
                let routes = routes.clone();
                tokio::spawn(async move {
                    let request = match Request::from_stream(&mut stream).await {
                        Ok(request) => request,
                        Err(err) => {
                            println!("Error occured during request parsing: {}", err);

                            return;
                        }
                    };

                    let response = match (routes.match_route(&request))(request).await {
                        Ok(response) => response,
                        Err(err) => {
                            println!("Error occured during request handling: {}", err);
                            let mut failure_response = Response::new();
                            failure_response.set_status(500);

                            failure_response
                        }
                    };

                    stream.write_all(&response.write()).await.unwrap();
                });
            }
        }
    }
}

#[derive(Default)]
pub struct ServerBuilder {
    routes: HashMap<String, Handler>,
}

impl ServerBuilder {
    pub fn new() -> ServerBuilder {
        let mut sb = ServerBuilder {
            routes: HashMap::new(),
        };

        sb.routes.insert(
            "default".to_string(),
            Box::new(|_| {
                Box::pin(async {
                    let mut response = Response::new();
                    response.set_status(404);

                    Ok(response)
                })
            }),
        );

        sb
    }

    pub fn add_route<F, Fut>(
        mut self,
        method: impl Into<String> + std::fmt::Display,
        path: impl Into<String> + std::fmt::Display,
        handler: F,
    ) -> ServerBuilder
    where
        F: 'static + Fn(Request) -> Fut + Sync + Send,
        Fut: 'static + Future<Output = Result<Response, Box<dyn std::error::Error>>> + Sync + Send,
    {
        self.routes.insert(
            format!("{}::{}", method, path),
            Box::new(move |arg| Box::pin(handler(arg))),
        );

        self
    }

    pub fn set_default_route<F, Fut>(mut self, handler: F) -> ServerBuilder
    where
        F: 'static + Fn(Request) -> Fut + Sync + Send,
        Fut: 'static + Future<Output = Result<Response, Box<dyn std::error::Error>>> + Sync + Send,
    {
        self.routes.insert(
            "default".to_string(),
            Box::new(move |arg| Box::pin(handler(arg))),
        );

        self
    }

    pub fn build(self, address: &str, port: u16) -> Result<Server, Box<dyn std::error::Error>> {
        let r = Routes {
            routes: self.routes,
        };

        let server = Server {
            routes: r,
            address: address.into(),
            port,
        };

        Ok(server)
    }
}
