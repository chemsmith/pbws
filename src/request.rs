use httparse::Status;
use tokio::io::AsyncReadExt;

const READ_BUFFER_SIZE: usize = 1024;
const MAX_BUFFER_FILL_ATTEMPTS: usize = 100;
const MAX_HEADER_COUNT: usize = 64;

#[derive(Debug)]
pub struct Request {
    method: String,
    path: String,
    headers: Vec<(String, String)>,
    body: Vec<u8>,
}

impl Request {
    pub async fn from_stream(
        mut stream: impl AsyncReadExt + Unpin,
    ) -> Result<Request, Box<dyn std::error::Error>> {
        let mut attempts_to_read = 0;
        let mut payload: Vec<u8> = Vec::new();

        let request: Request = loop {
            attempts_to_read += 1;
            if attempts_to_read > MAX_BUFFER_FILL_ATTEMPTS {
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "Max attempts to read reached",
                )));
            }
            let mut buffer = [0; READ_BUFFER_SIZE];
            let bytes_read = stream.read(&mut buffer).await.unwrap();
            let mut headers = [httparse::EMPTY_HEADER; MAX_HEADER_COUNT];
            let mut req = httparse::Request::new(&mut headers);
            payload.extend_from_slice(&buffer[..bytes_read]);

            match req.parse(&payload) {
                Ok(Status::Complete(headers_len)) => {
                    let mut content_length = 0;
                    let mut header_vec = Vec::new();
                    for header in req.headers {
                        if header.name == "Content-Length" {
                            content_length = String::from_utf8_lossy(header.value)
                                .parse::<usize>()
                                .unwrap();
                        }
                        header_vec.push((
                            header.name.to_string(),
                            String::from_utf8_lossy(header.value).to_string(),
                        ));
                    }
                    if (content_length + headers_len) > payload.len() {
                        continue;
                    }
                    break Request::new(
                        req.method.unwrap(),
                        req.path.unwrap(),
                        header_vec,
                        payload[headers_len..].to_vec(),
                    );
                }
                Ok(Status::Partial) => {
                    continue;
                }
                Err(err) => {
                    return Err(Box::new(err));
                }
            }
        };

        Ok(request)
    }

    fn new(
        method: impl Into<String>,
        path: impl Into<String>,
        headers: Vec<(String, String)>,
        body: impl Into<Vec<u8>>,
    ) -> Request {
        Request {
            method: method.into(),
            path: path.into(),
            headers,
            body: body.into(),
        }
    }

    pub fn method(&self) -> &str {
        &self.method
    }

    pub fn path(&self) -> &str {
        self.path.split('?').collect::<Vec<&str>>()[0]
    }

    pub fn headers(&self) -> &[(String, String)] {
        &self.headers
    }

    pub fn header(&self, name: impl Into<String>) -> Option<&String> {
        let name = name.into();
        for (header_name, header_value) in &self.headers {
            if header_name == &name {
                return Some(header_value);
            }
        }

        None
    }

    pub fn body(&self) -> &[u8] {
        &self.body
    }

    pub fn body_as_string(&self) -> String {
        String::from_utf8_lossy(&self.body).to_string()
    }
}

#[cfg(test)]
mod test {
    use crate::request::Request;
    
    #[test]
    fn test_request() {
        let request = Request::new(
            "GET",
            "/test",
            vec![("Content-Length".to_string(), "5".to_string())],
            "Hello".to_string(),
        );

        assert_eq!(request.method(), "GET");
        assert_eq!(request.path(), "/test");
        assert_eq!(request.headers().len(), 1);
        assert_eq!(request.header("Content-Length").unwrap(), "5");
        assert_eq!(request.body(), "Hello".as_bytes());
        assert_eq!(request.body_as_string(), "Hello");
    }

    #[tokio::test]
    async fn test_request_from_stream() {
        let mut request_bytes = "GET /test HTTP/1.1\r\nContent-Length: 5\r\n\r\nHello".as_bytes();

        let request = Request::from_stream(&mut request_bytes).await.unwrap();

        assert_eq!(request.method(), "GET");
        assert_eq!(request.path(), "/test");
        assert_eq!(request.headers().len(), 1);
        assert_eq!(request.header("Content-Length").unwrap(), "5");
        assert_eq!(request.body(), "Hello".as_bytes());
        assert_eq!(request.body_as_string(), "Hello");
    }

    #[tokio::test]
    async fn test_request_from_stream_bad_content_length() {
        let mut request_bytes = "GET /test HTTP/1.1\r\nContent-Length: 10\r\n\r\nHello".as_bytes();

        let request_err = Request::from_stream(&mut request_bytes).await.unwrap_err();

        assert_eq!(
            request_err.to_string(),
            "Max attempts to read reached"
        );
    }
}
